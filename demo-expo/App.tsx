import axios, { AxiosResponse } from 'axios';
import React, { useEffect, useState } from 'react'
import { Text, View } from 'react-native'
import SelectBox, { Item } from 'react-native-multi-selectbox-typescript'

// API URL for getting data such as provinces, districts and communes
const provinceAPIUrl = 'https://api.mysupership.vn/v1/partner/areas/province';
const districtAPIUrl = 'https://api.mysupership.vn/v1/partner/areas/district';
const communeAPIUrl = 'https://api.mysupership.vn/v1/partner/areas/commune';

// Format type of receiving data from API
type data = {
  code: string,
  name: string
};

type dataList = {
  results: data[]
};

function App() {
  // Nessesary useState declaration
  const [provinces, setProvinces] = useState<Item[]>();
  const [province, setProvince] = useState<Item>();
  const [districts, setDistricts] = useState<Item[]>();
  const [district, setDistrict] = useState<Item>();
  const [commune, setCommune] = useState<Item>();
  const [communes, setCommunes] = useState<Item[]>();

  // Get all provinces when load page
  useEffect(() => {
    axios.get<dataList>(provinceAPIUrl).then((response: AxiosResponse<dataList>) => {
      let tempResult: Item[] = response.data.results.map((result) => {
        // Options data must contain 'item' & 'id' keys
        return {
          item: result.name,
          id: result.code,
        }
      });
      setProvinces(tempResult);
    })
  }, []);

  // Get all the districts of one choosen province
  useEffect(() => {
    setDistricts(undefined);
    setDistrict(undefined);
    province && axios.get<dataList>(districtAPIUrl, { params: { 'province': province.id } }).then((response: AxiosResponse<dataList>) => {
      let tempResult: Item[] = response.data.results.map((result) => {
        // Options data must contain 'item' & 'id' keys
        return {
          item: result.name,
          id: result.code,
        }
      });
      setDistricts(tempResult);
    })
  }, [province]);

  // Get all the commune of one choosen district
  useEffect(() => {
    setCommunes(undefined);
    setCommune(undefined);
    district && axios.get<dataList>(communeAPIUrl, { params: { 'district': district.id } }).then((response: AxiosResponse<dataList>) => {
      let tempResult: Item[] = response.data.results.map((result) => {
        // Options data must contain 'item' & 'id' keys
        return {
          item: result.name,
          id: result.code,
        }
      });
      setCommunes(tempResult);
    })
  }, [district]);

  return (
    <View style={{ margin: 30 }}>
      <View style={{ width: '100%', alignItems: 'center' }}>
        <Text style={{ fontSize: 30, paddingBottom: 20 }}>Demos</Text>
      </View>
      <Text style={{ fontSize: 20, paddingBottom: 10 }}>Select Demo</Text>
      <SelectBox
        label="Select Province"
        options={provinces}
        value={province}
        hideInputFilter={true}
        onChange={(val: Item) => setProvince(val)}
      />

      {/* If there is district list and it's length is not zero then render it's multi-selected box*/}
      {districts && districts.length != 0 ? <SelectBox
        label="Select District"
        options={districts}
        value={district}
        hideInputFilter={true}
        onChange={(val: Item) => setDistrict(val)}
      /> : ''}

      {/* If there is commune list and it's length is not zero then render it's multi-selected box */}
      {communes && communes.length != 0 ? <SelectBox
        label="Select Commune"
        options={communes}
        value={commune}
        hideInputFilter={true}
        onChange={(val: Item) => setCommune(val)}
      /> : ''}
    </View>
  )

}

export default App